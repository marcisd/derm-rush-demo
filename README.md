# Derm Rush Demo #

### Disclaimer ###

This is for demo purposes only. This was done in 2015 and some functionalities may not work properly when rebuilt.

### About Derm Rush ###

Derm Rush is a project of XS Multimedia for Bayer HealthCare Pharmaceuticals. It is a clone of the game Dumb Ways To Die and is comprised of multiple minigames.

### References ###

* [Dumb Ways To Die](http://www.dumbwaystodie.com/)