﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;



[System.Serializable]
public class TouchEvent : UnityEvent<int, Vector2>{}

[System.Serializable]
public class TouchDeltaEvent : UnityEvent<int, Vector2, Vector2>{}

public class TouchInput : MonoBehaviour {

	[SerializeField]
	public TouchEvent TouchBegan;
	[SerializeField]
	public TouchDeltaEvent TouchMoved;
	[SerializeField]
	public TouchEvent TouchEnded;

	// Controls
	private string selectButton = "Fire1";       // Button used for interacting with the Board

	// Input
	Vector3 _previousMousePosition;              // Mouse position during the previous update step

	void Update()
	{
		UpdateInput();
	}

	void UpdateInput()
	{
		#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER

		if (Input.GetButtonDown(selectButton))
		{
			_previousMousePosition = Input.mousePosition;
			HandleTouch(-1, Input.mousePosition, TouchPhase.Began, Vector3.zero, Time.deltaTime);
		}
		else if (Input.GetButton(selectButton))
		{
			if (Input.GetAxisRaw("Mouse X") != 0 || Input.GetAxisRaw("Mouse Y") != 0)
			{
				HandleTouch(-1, Input.mousePosition, TouchPhase.Moved, Input.mousePosition - _previousMousePosition, Time.deltaTime);
				_previousMousePosition = Input.mousePosition;
			}
			else
			{
				HandleTouch(-1, Input.mousePosition, TouchPhase.Stationary, Vector2.zero, Time.deltaTime);
			}
		}
		else if (Input.GetButtonUp(selectButton))
		{
			HandleTouch(-1, Input.mousePosition, TouchPhase.Ended, Vector2.zero, Time.deltaTime);
		}

		#else

		if (Input.touchCount > 0)
		{
			foreach (Touch touch in Input.touches)
			{
				HandleTouch(touch.fingerId, touch.position, touch.phase, touch.deltaPosition, touch.deltaTime);
			}
		}

		#endif
	}

	private void HandleTouch(int touchID, Vector2 position, TouchPhase phase, Vector2 deltaPosition, float deltaTime)
	{
		if (true)
		{
			switch (phase)
			{
			case TouchPhase.Began:
				TouchBegan.Invoke (touchID, position);
				break;
			case TouchPhase.Moved:
				TouchMoved.Invoke (touchID, position, deltaPosition);
				break;
			case TouchPhase.Ended:
				TouchEnded.Invoke (touchID, position);
				break;
			default:
				break;
			}
		}
	}

}