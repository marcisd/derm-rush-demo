﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
		if (GameManager.Instance.BestScore < GameManager.Instance.totalScore) {
			GameManager.Instance.BestScore = GameManager.Instance.totalScore;
		}
	}

	public void BackToMainMenu () {
		Application.LoadLevel ("scn_MainMenu");
	}
		
	public void PlayAgain () {
		GameManager.Instance.Init ();
		Application.LoadLevel ("scn_Transition");
	}
}
