﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TransitionScript : MonoBehaviour {

	public Text scoreLabel;
	public float runningTime = 3;

	public GameObject Win;
	public GameObject Lose;

	public GameObject winChar;
	public GameObject loseChar;

	public GameObject winText;
	public GameObject loseText;

	public GameObject ready;
	public GameObject world;

	private Vector2 startV = new Vector2 (0f, -20f);
	private Vector2 endWinV = new Vector2 (0f, -6.5f);
	private Vector2 endLoseV = new Vector2 (0f, -7f);

	public Action OnCorrect;
	public Action OnWrong;

	private int _startingScore;
	private int _finalScore;
	private float _runningTime;

	[HideInInspector]
	public bool firstPlay = false;

	// Use this for initialization
	void Start () {
		_finalScore = GameManager.Instance.totalScore;
		_startingScore = _finalScore - GameManager.Instance.CurrentScore;
		_runningTime = runningTime;

		scoreLabel.text = _startingScore.ToString ();

		if (GameManager.Instance.GetCurrentLevelIndex () < 1) {
			// ready!!!
			ready.SetActive (true);
			world.SetActive (false);
			scoreLabel.gameObject.SetActive (false);
		} else {
			ready.SetActive (false);
			world.SetActive (true);	
			scoreLabel.gameObject.SetActive (true);
		}
	}
	
	// Update is called once per frame
	void Update () {

		_runningTime -= Time.deltaTime;

		if (_runningTime < 0) {
			if (GameManager.Instance.HasNextLevel ()) {
				Application.LoadLevel (GameManager.Instance.GetLevelAndIncrement ());
			} else {
				Application.LoadLevel ("scn_GameOver");
			}
			return;
		}

		if (GameManager.Instance.GetCurrentLevelIndex () < 1) {
			return;
		}

		if (!firstPlay) {
			firstPlay = true;

			if (GameManager.Instance.CurrentScore > 0) {
				if (OnCorrect != null && OnCorrect.GetInvocationList ().Length > 0) {
					OnCorrect.Invoke ();
				}
			} else {
				if (OnWrong != null && OnWrong.GetInvocationList ().Length > 0) {
					OnWrong.Invoke ();
				}
			}
		}

		float completion = 0f;
		float transCompletion = 0f;
		if (_runningTime < runningTime - 1.5f) {
			completion = 1f;
			transCompletion = 1f;
		} else if (_runningTime < runningTime - 0.5f) {
			completion = (_runningTime - 1.5f) / 1f;
			transCompletion = 1f;
		} else if (_runningTime < runningTime) {
			completion = 0f;
			transCompletion = 1 - ((_runningTime - 2.5f) / 0.5f);
		}

		Transform tref = null;
		if (GameManager.Instance.CurrentScore > 0) {
			Win.SetActive (true);
			Lose.SetActive (false);
			winText.SetActive (true);
			loseText.SetActive (false);
			winChar.transform.localPosition = Vector2.Lerp (startV, endWinV, transCompletion);
			tref = GameObject.Find ("board_win").transform;
		} else {
			Win.SetActive (false);
			Lose.SetActive (true);
			winText.SetActive (false);
			loseText.SetActive (true);
			loseChar.transform.localPosition = Vector2.Lerp (startV, endLoseV, transCompletion);
			tref = GameObject.Find ("board_lose").transform;
		}

		RectTransform CanvasRect=GameObject.Find ("Canvas").GetComponent<RectTransform>();
		Vector2 ViewportPosition=Camera.main.WorldToViewportPoint(tref.position);
		Vector2 WorldObject_ScreenPosition=new Vector2(
			((ViewportPosition.x*CanvasRect.sizeDelta.x)-(CanvasRect.sizeDelta.x*0.5f)),
			((ViewportPosition.y*CanvasRect.sizeDelta.y)-(CanvasRect.sizeDelta.y*0.5f)));

		scoreLabel.rectTransform.anchoredPosition = WorldObject_ScreenPosition;

		scoreLabel.text = ((int)Mathf.Lerp (_startingScore, _finalScore, completion)).ToString ();
	}
}
