﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoSingleton<GameManager> {

	private readonly List<string> kMiniGameList = new List<string> () {
		"scn_PillFlick",
		"scn_PimpleTap",
		"scn_FootRub",
		"scn_Arm",
		"scn_Multiple",
		"scn_TrueFalse",
		"scn_Spelling",
		"scn_Balloon",
	};

	private List<string> _shuffledList;
	private int _currentLevel;
	private int _currentScore;

	public int totalScore;

	public int CurrentScore {
		get {
			return _currentScore;
		}
		set {
			_currentScore = value;
			totalScore += value;
		}
	}

	// Use this for initialization
	public override void Init ()
	{
		_shuffledList = new List<string> (kMiniGameList);
		_shuffledList.Shuffle <string> ();

		totalScore = 0;
		_currentLevel = 0;
		_currentScore = 0;
	}

	public bool HasNextLevel () {
		return _currentLevel < _shuffledList.Count;
	}

	public int GetCurrentLevelIndex () {
		return _currentLevel;
	}

	public string GetLevelAndIncrement () {
		if (!HasNextLevel ())
			return "";

		return _shuffledList [_currentLevel++];
	}

	public int BestScore {
		get {
			if (PlayerPrefs.HasKey ("bestScore")) {
				return PlayerPrefs.GetInt ("bestScore");
			} else {
				return 0;
			}
		}
		set {
			PlayerPrefs.SetInt ("bestScore", value);
		}
	}
}
