﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Prime31.Sound;

public class MultipleAudioManager : MonoBehaviour {

	public List<AudioClip> click;

	public AudioClip wrong;
	public AudioClip correct;

	MultipleChoiceMinigame _minigame;
	System.Random _rng = new System.Random ();

	// Use this for initialization
	void Start () {
		_minigame = gameObject.GetComponent <MultipleChoiceMinigame> ();

		_minigame.OnCorrect += PlayCorrect;
		_minigame.OnWrong += PlayWrong;
	}

	void PlayCorrect () {
		int rand = _rng.Next (click.Count);

		SoundKit.instance.playSound (click [rand]);
		SoundKit.instance.playSound (correct);
	}

	void PlayWrong () {
		int rand = _rng.Next (click.Count);

		SoundKit.instance.playSound (click [rand]);
		SoundKit.instance.playSound (wrong);
	}
}
