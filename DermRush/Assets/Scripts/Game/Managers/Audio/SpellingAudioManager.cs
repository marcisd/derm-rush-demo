﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Prime31.Sound;

public class SpellingAudioManager : MonoBehaviour {

	public List<AudioClip> beep;

	public AudioClip wrong;
	public AudioClip correct;

	SpellingMinigame _minigame;
	System.Random _rng = new System.Random ();

	// Use this for initialization
	void Start () {
		_minigame = gameObject.GetComponent <SpellingMinigame> ();

		_minigame.OnBeep += PlayBeep;

		_minigame.OnCorrect += PlayCorrect;
		_minigame.OnWrong += PlayWrong;
	}

	void PlayBeep () {

		int rand = _rng.Next (beep.Count);

		SoundKit.instance.playSound (beep [rand]);
	}

	void PlayCorrect () {
		SoundKit.instance.playSound (correct);
	}

	void PlayWrong () {
		SoundKit.instance.playSound (wrong);
	}
}
