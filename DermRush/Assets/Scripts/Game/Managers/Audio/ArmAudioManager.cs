﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Prime31.Sound;

public class ArmAudioManager : MonoBehaviour {

	public List<AudioClip> swipe;

	ArmMinigame _minigame;
	System.Random _rng = new System.Random ();

	bool _isPlayingLaugh;
	bool _isPlayungSwipe;

	// Use this for initialization
	void Start () {

		_minigame = gameObject.GetComponent <ArmMinigame> ();

		_minigame.OnBrush += PlayBrush;

		_isPlayungSwipe = false;
	}

	void PlayBrush () {

		if (!_isPlayungSwipe) {
			_isPlayungSwipe = true;
			int rand = _rng.Next (swipe.Count);
			SoundKit.instance.playSound (swipe [rand]).setCompletionHandler (() => {
				_isPlayungSwipe = false;
			});
		}
	}
}
