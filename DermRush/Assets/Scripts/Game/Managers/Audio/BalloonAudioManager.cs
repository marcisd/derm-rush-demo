﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Prime31.Sound;

public class BalloonAudioManager : MonoBehaviour {

	public List<AudioClip> elastic;

	public AudioClip wrong;
	public AudioClip correct;

	bool isFinishSoundPlaying;

	BalloonMinigame _minigame;
	System.Random _rng = new System.Random ();

	// Use this for initialization
	void Start () {
		_minigame = gameObject.GetComponent <BalloonMinigame> ();

		_minigame.OnHoldBalloon += PlayElastic;

		_minigame.OnCorrect += PlayCorrect;
		_minigame.OnWrong += PlayWrong;

		isFinishSoundPlaying = false;
	}

	void PlayElastic () {
		int rand = _rng.Next (elastic.Count);

		SoundKit.instance.playSound (elastic [rand]);
	}
	
	void PlayCorrect () {
		if (!isFinishSoundPlaying) {
			isFinishSoundPlaying = true;
			SoundKit.instance.playSound (correct).setCompletionHandler (() => {
				isFinishSoundPlaying = false;	
			});
		}

	}

	void PlayWrong () {
		if (!isFinishSoundPlaying) {
			isFinishSoundPlaying = true;
			SoundKit.instance.playSound (wrong).setCompletionHandler (() => {
				isFinishSoundPlaying = false;	
			});
		}
	}
}
