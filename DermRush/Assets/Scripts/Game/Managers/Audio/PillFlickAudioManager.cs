﻿using UnityEngine;
using System.Collections;
using Prime31.Sound;
using System.Collections.Generic;

public class PillFlickAudioManager : MonoBehaviour {

	public List<AudioClip> allergy;

	[Space]
	public AudioClip flick;

	PillFlickMinigame _minigame;
	System.Random _rng = new System.Random ();

	// Use this for initialization
	void Start () {
		_minigame = gameObject.GetComponent <PillFlickMinigame> ();

		_minigame.OnFlick += PlayFlick;
		_minigame.OnAllergy += PlayAllergy;
	}
	
	void PlayAllergy () {

		int rand = _rng.Next (allergy.Count);

		SoundKit.instance.playSound (allergy [rand]);
	}

	void PlayFlick () {
		SoundKit.instance.playSound (flick);
	}
}
