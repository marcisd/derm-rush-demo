﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Prime31.Sound;

public class TrueFalseAudioManager : MonoBehaviour {

	public List<AudioClip> click;

	public AudioClip wrong;
	public AudioClip correct;

	TrueFalseMinigame _minigame;
	System.Random _rng = new System.Random ();

	// Use this for initialization
	void Start () {
		_minigame = gameObject.GetComponent <TrueFalseMinigame> ();

		_minigame.OnCorrect += PlayCorrect;
		_minigame.OnWrong += PlayWrong;
	}

	void PlayCorrect () {
		int rand = _rng.Next (click.Count);

		SoundKit.instance.playSound (click [rand]);
		SoundKit.instance.playSound (correct);

	}

	void PlayWrong () {
		int rand = _rng.Next (click.Count);

		SoundKit.instance.playSound (click [rand]);
		SoundKit.instance.playSound (wrong);
	}
}
