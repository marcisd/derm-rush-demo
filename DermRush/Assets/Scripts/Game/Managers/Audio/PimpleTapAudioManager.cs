﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Prime31.Sound;

public class PimpleTapAudioManager : MonoBehaviour {

	public List<AudioClip> pops;

	PimpleTapMinigame _minigame;
	System.Random _rng = new System.Random ();

	// Use this for initialization
	void Start () {
		_minigame = gameObject.GetComponent <PimpleTapMinigame> ();

		_minigame.OnPop += PlayPop;
	}

	void PlayPop () {

		int rand = _rng.Next (pops.Count);

		SoundKit.instance.playSound (pops [rand]);
	}
}
