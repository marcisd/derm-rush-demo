﻿using UnityEngine;
using System.Collections;
using Prime31.Sound;

public class TransitionAudioManager : MonoBehaviour {

	public AudioClip wrong;
	public AudioClip correct;

	TransitionScript transition;

	// Use this for initialization
	void Start () {
		transition = gameObject.GetComponent <TransitionScript> ();

		transition.OnCorrect += PlayCorrect;
		transition.OnWrong += PlayWrong;
	}
	
	void PlayCorrect () {
		SoundKit.instance.playSound (correct);
	}

	void PlayWrong () {
		SoundKit.instance.playSound (wrong);
	}
}
