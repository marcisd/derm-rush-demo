﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Prime31.Sound;

public class FootRubAudioManager : MonoBehaviour {

	public List<AudioClip> laugh;
	[Space]
	public List<AudioClip> swipe;

	FootRubMinigame _minigame;
	System.Random _rng = new System.Random ();

	bool _isPlayingLaugh;
	bool _isPlayungSwipe;

	// Use this for initialization
	void Start () {
	
		_minigame = gameObject.GetComponent <FootRubMinigame> ();

		_minigame.OnBrush += PlayBrush;

		_isPlayingLaugh = false;
		_isPlayungSwipe = false;
	}
	
	void PlayBrush () {

		if (!_isPlayungSwipe) {
			_isPlayungSwipe = true;
			int rand = _rng.Next (swipe.Count);
			SoundKit.instance.playSound (swipe [rand]).setCompletionHandler (() => {
				_isPlayungSwipe = false;
			});
		}

		if (!_isPlayingLaugh) {
			_isPlayingLaugh = true;
			int rand = _rng.Next (laugh.Count);
			SoundKit.instance.playSound (laugh [rand]).setCompletionHandler (() => {
				_isPlayingLaugh = false;
			});
		}
	}
}
