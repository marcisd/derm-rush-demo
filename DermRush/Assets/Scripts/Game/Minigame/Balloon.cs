﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Balloon : MonoBehaviour {

	public bool spinReverse = false;
	public float startingangle = 0;

	float spinRadius = 0.3f;
	float angle = 0;

	// Use this for initialization
	void Start () {
		angle = startingangle;
	}

	// Update is called once per frame
	void Update () {

		angle = (angle += Time.deltaTime * 2) % 360f;

		float y = spinRadius * Mathf.Sin (angle);

		RectTransform rt = gameObject.GetComponent <RectTransform> ();
		if (spinReverse)
			rt.localEulerAngles = rt.localEulerAngles + (Vector3.back * y);
		else
			rt.localEulerAngles = rt.localEulerAngles + (Vector3.forward * y);
	}

	public GameObject GetStringEnd () {
		foreach (RectTransform child in transform as RectTransform) {
			if (child.gameObject.name == "StringEnd")
				return child.gameObject;
		}

		return null;
	}

	public void SetText (string text) {
		var t = gameObject.GetComponentInChildren <Text> ();
		t.text = text.ToUpper ();
	}

	public string GetText () {
		var t = gameObject.GetComponentInChildren <Text> ();
		return t.text;
	}
}
