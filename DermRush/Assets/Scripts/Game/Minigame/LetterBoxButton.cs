﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LetterBoxButton : LetterBox {

	public SpellingMinigame spellingMinigame;
	bool _isSet = false;

	public void SendInput () {
		if (_isSet)
			return;

		spellingMinigame.AcceptInput (this);
	}

	public void MarkAsSet () {
		_isSet = true;
		gameObject.GetComponent <Button> ().interactable = false;
	}
}
