﻿using UnityEngine;
using System.Collections;

public class ArmKetongSection : MonoBehaviour {

	public int GetKetongCount () {
		ArmKetong[] ketongs = transform.GetComponentsInChildren<ArmKetong>();
		return ketongs.Length;
	}
}
