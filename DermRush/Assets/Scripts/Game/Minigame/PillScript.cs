﻿using UnityEngine;
using System.Collections;

public class PillScript : MonoBehaviour {

	public bool isFired;

	// Use this for initialization
	void Start () {
		isFired = false;
	}

	public IEnumerator MoveTo (Vector2 direction) {

		Vector3 dir = new Vector3 (direction.x, direction.y, 0).normalized;
		float timer = 10f;


		while (timer > 0) {
			timer -= Time.deltaTime;
			transform.Translate (dir);
			yield return new WaitForFixedUpdate();
		}

		Destroy (gameObject);

		yield return null;
	}
}
