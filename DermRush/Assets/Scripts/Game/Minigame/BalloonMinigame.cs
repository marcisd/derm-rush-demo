﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BalloonMinigame : AbstractMinigame {

	public List <string> labels;
	public string correctLabel;

	public Action OnHoldBalloon;

	public Action OnCorrect;
	public Action OnWrong;

	bool _isHoldingBalloon;

	GameObject _stringEnd;
	LineRenderer _string;

	Vector3 _stringOtherEnd;
	string _currentSelected = "";

	public List <string> _shuffledList;

	// Use this for initialization
	void Start () {
		StartTime ();

		_shuffledList = new List<string> (labels);
		_shuffledList.Shuffle <string> ();

		_isHoldingBalloon = false;

		_string = GameObject.Find ("String").GetComponent <LineRenderer> ();
		_string.gameObject.SetActive (false);

		int i = 0;
		foreach (GameObject balloon in GameObject.FindGameObjectsWithTag ("Balloon")) {
			if (i < _shuffledList.Count)
				balloon.GetComponent <Balloon> ().SetText (_shuffledList [i++]);
		}

	}
	
	// Update is called once per frame
	void Update () {
		UpdateTime ();
	
		if (_string.gameObject.activeSelf) {
			_string.SetPosition (0, _stringEnd.transform.position);
			_string.SetPosition (1, _stringOtherEnd);
		}
	}

	public override void OnTimeEndEvent () {
		OnConditionFailEvent ();
	}

	public void TouchBegan (int idx, Vector2 position) {

		if (!acceptsInput)
			return;

		var ray = Camera.main.ScreenPointToRay(position);
		RaycastHit2D hit2d = Physics2D.GetRayIntersection(ray,Mathf.Infinity);

		if (hit2d.collider) {
			if (hit2d.collider.gameObject.tag == "Balloon") {

				if (OnHoldBalloon != null && OnHoldBalloon.GetInvocationList ().Length > 0) {
					OnHoldBalloon.Invoke ();
				}

				_isHoldingBalloon = true;
				_stringEnd = hit2d.collider.gameObject.GetComponent <Balloon> ().GetStringEnd ();
				_currentSelected = hit2d.collider.gameObject.GetComponent <Balloon> ().GetText ();
				_string.gameObject.SetActive (true);
				_stringOtherEnd = Camera.main.ScreenToWorldPoint (position);
			}
		}

	}

	public void TouchMoved (int idx, Vector2 position, Vector2 delta) {

		if (!acceptsInput)
			return;

		_stringOtherEnd = Camera.main.ScreenToWorldPoint (position);

		var ray = Camera.main.ScreenPointToRay(position);
		RaycastHit2D hit2d = Physics2D.GetRayIntersection(ray,Mathf.Infinity);

		if (hit2d.collider) {
			if (hit2d.collider.gameObject.tag == "Target") {

				if (String.Equals (_currentSelected.ToUpper (), correctLabel.ToUpper ())) {

					if (OnCorrect != null && OnCorrect.GetInvocationList ().Length > 0) {
						OnCorrect.Invoke ();
					}

					OnConditionSuccessEvent ();
				} else {

					if (OnWrong != null && OnWrong.GetInvocationList ().Length > 0) {
						OnWrong.Invoke ();
					}


					OnConditionTimedFailEvent ();
				}
			}
		}
	}

	public void TouchEnded (int idx, Vector2 position) {

		if (!acceptsInput)
			return;

		if (_isHoldingBalloon) {

			_string.gameObject.SetActive (false);

			_isHoldingBalloon = false;
			_stringEnd = null;
			_currentSelected = "";
		}
	}
}
