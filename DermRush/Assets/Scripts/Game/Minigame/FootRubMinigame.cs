﻿using UnityEngine;
using System.Collections;
using System;

public class FootRubMinigame : AbstractMinigame {

	public GameObject brushPrefab;
	public GameObject ketongMask;

	public GameObject leftFootChecks;
	public GameObject rightFootChecks;

	public Action OnBrush;

	// Use this for initialization
	void Start () {
		StartTime ();
	}

	// Update is called once per frame
	void Update () {
		UpdateTime ();

		RectTransform[] lfc = leftFootChecks.GetComponentsInChildren<RectTransform>();
		RectTransform[] rfc = rightFootChecks.GetComponentsInChildren<RectTransform>();
		if (lfc.Length == 1 && rfc.Length == 1) {
			OnConditionSuccessEvent ();
		}

	}

	public override void OnTimeEndEvent () {
		OnConditionFailEvent ();
	}

	public void PaintBrush (Vector2 position) {

		if (!acceptsInput)
			return;

		if (OnBrush != null && OnBrush.GetInvocationList ().Length > 0) {
			OnBrush.Invoke ();
		}

		Vector2 pos;
		RectTransform ketongMaskRect = ketongMask.transform as RectTransform;
		RectTransformUtility.ScreenPointToLocalPointInRectangle (ketongMaskRect, position, Camera.main, out pos);

		GameObject go = Instantiate (brushPrefab, pos, Quaternion.identity) as GameObject;
		go.transform.SetParent (ketongMask.transform, false);
	}

	public void TouchBegan (int idx, Vector2 position) {

		PaintBrush (position);
	}

	public void TouchMoved (int idx, Vector2 position, Vector2 delta) {

		PaintBrush (position);
	}

	public void TouchEnded (int idx, Vector2 position) {

	}
}
