﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class SpellingMinigame : AbstractMinigame {

	public string mysteryWord;
	public int numberOfBlanks;

	public GameObject button;
	public GameObject normal;
	public GameObject blank;

	List<LetterBox> _mysteryTiles;
	List<int> _blankIndeces;

	public Action OnBeep;

	public Action OnCorrect;
	public Action OnWrong;

	// Use this for initialization
	void Start () {
		StartTime ();

		_mysteryTiles = new List<LetterBox> ();
		_blankIndeces = new List<int> ();

		if (numberOfBlanks > 0) {
			System.Random rng = new System.Random ();

			do {
				int r = rng.Next (mysteryWord.Length);

				if (mysteryWord.ToCharArray () [r] == ' ')
					continue;

				if (_blankIndeces.Contains (r))
					continue;

				_blankIndeces.Add (r);
			} while (_blankIndeces.Count <= Mathf.Min(numberOfBlanks, mysteryWord.Length) - 1);

			foreach (int idx in _blankIndeces) {
				CreateButtonWithLetter (mysteryWord.ToCharArray () [idx]);
			}

		}

		int i = 0;
		bool secondLine = false;
		foreach (char c in mysteryWord.ToCharArray ()) {
			if (c != ' ') {
				Transform container;
				if (!secondLine) {
					container = GameObject.Find ("Tiles2").transform;
				} else {
					container = GameObject.Find ("Tiles3").transform;
				}

				if (_blankIndeces.Contains (i)) {
					CreateMysteryTile (c, container);
				} else {
					CreateOpenTile (c, container);
				}
			} else {
				secondLine = true;
			}

			i++;
		}

		ArrangeTiles (GameObject.Find ("Tiles"));
		ArrangeTiles (GameObject.Find ("Tiles2"));
		ArrangeTiles (GameObject.Find ("Tiles3"));

	}

	void CreateButtonWithLetter (char letter) {
		Button b = Instantiate (button).GetComponent <Button> ();
		b.transform.SetParent (GameObject.Find ("Tiles").transform, false);
		b.GetComponent <LetterBoxButton> ().spellingMinigame = this;
		b.GetComponent <LetterBoxButton> ().Letter = letter;
		b.onClick.AddListener (() => { b.GetComponent <LetterBoxButton> ().SendInput (); });
	}

	void CreateMysteryTile (char letter, Transform parent) {
		GameObject go = Instantiate (blank);
		go.transform.SetParent (parent, false);
		go.GetComponent <LetterBox> ().Letter = letter;
		_mysteryTiles.Add (go.GetComponent <LetterBox> ());
	}

	void CreateOpenTile (char letter, Transform parent) {
		GameObject go = Instantiate (normal);
		go.transform.SetParent (parent, false);
		go.GetComponent <LetterBox> ().Letter = letter;
	}

	private RectTransform[] GetFirstChildren(RectTransform parent){
		RectTransform[] children = parent.GetComponentsInChildren<RectTransform>();
		RectTransform[] firstChildren = new RectTransform[parent.childCount];
		int index = 0;
		foreach (RectTransform child in children){
			if (child.parent == parent){
				firstChildren[index] = child;
				index++;
			}
		}
		return firstChildren;
	}

	void ArrangeTiles (GameObject tileContainer) {

		int count = tileContainer.transform.childCount;

		if (count <= 1)
			return;
		
		float space = 50f;
		float startingOffset = space * (count - 1) / 2;

		RectTransform[] firstChildren = GetFirstChildren (tileContainer.transform as RectTransform);

		int i = 0;
		foreach (RectTransform tile in firstChildren) {
			tile.localPosition = new Vector3 (-startingOffset + (space * i++), tile.localPosition.y, tile.localPosition.z);
		}
	}
	
	// Update is called once per frame
	void Update () {
		UpdateTime ();
	}

	public override void OnTimeEndEvent () {
		OnConditionFailEvent ();
	}

	public void AcceptInput (LetterBoxButton letterbox) {
		Debug.Log ("Input from letterbox with letter : " + letterbox.Letter);

		if (_mysteryTiles.Count <= 0)
			return;

		if (OnBeep != null && OnBeep.GetInvocationList ().Length > 0) {
			OnBeep.Invoke ();
		}

		if (letterbox.LetterMatches (_mysteryTiles [0].Letter)) {
			Debug.Log ("MATCH");

			RectTransform sourceTile = letterbox.gameObject.GetComponent <RectTransform> ();
			RectTransform destinationTile = _mysteryTiles [0].gameObject.GetComponent <RectTransform> ();

			Vector3 posInDestination = destinationTile.localPosition + destinationTile.parent.localPosition - sourceTile.parent.localPosition;
			posInDestination += Vector3.forward * sourceTile.localPosition.z;

			StartCoroutine (MoveFromToDurationFactor (sourceTile, sourceTile.localPosition, posInDestination, 0.2f));

			letterbox.MarkAsSet ();

			// remove first mystery tile
			_mysteryTiles.RemoveAt (0);

			if (_mysteryTiles.Count == 0) {

				if (OnCorrect != null && OnCorrect.GetInvocationList ().Length > 0) {
					OnCorrect.Invoke ();
				}

				OnConditionSuccessEvent ();
			}
		} else {
			Debug.Log ("NO MATCH");
		}
	}

	IEnumerator MoveFromToDurationFactor (RectTransform objectToMove, Vector3 source, Vector3 destination, float duration) {
		float startTime = Time.time;
		while (Time.time < startTime + duration) {
			float t = (Time.time - startTime) / duration;
			objectToMove.localPosition = Vector3.Lerp(source, destination, t); // Move objectToMove closer to b
			yield return new WaitForFixedUpdate();         // Leave the routine and return here in the next frame
		}
		objectToMove.localPosition = destination;
	}
}
