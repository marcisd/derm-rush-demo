﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = System.Random;
using System;

public class PimpleTapMinigame : AbstractMinigame {

	private const int kTotalTouchesToProcess = 2;

	public List<GameObject> set1;
	public List<GameObject> set2;
	public List<GameObject> set3;
	public List<GameObject> set4;
	public List<GameObject> set5;
	public List<GameObject> set6;
	public List<GameObject> set7;
	public List<GameObject> set8;
	public List<GameObject> set9;
	public List<GameObject> set10;

	private List<GameObject> objectSet;

	public Action OnPop;

	static Random rng = new Random ();

	void Awake () {
		objectSet = new List<GameObject> ();

		SelectRandomObject (set1);
		SelectRandomObject (set2);
		SelectRandomObject (set3);
		SelectRandomObject (set4);
		SelectRandomObject (set5);
		SelectRandomObject (set6);
		SelectRandomObject (set7);
		SelectRandomObject (set8);
		SelectRandomObject (set9);
		SelectRandomObject (set10);
	}

	void SelectRandomObject (IList list) {

		if (list.Count == 0)
			return;

		foreach (GameObject go in list) {
			go.SetActive (false);
		}
		int idx = rng.Next (list.Count);
		GameObject obj = list [idx] as GameObject;
		obj.SetActive (true);
		objectSet.Add (obj);
	}

	// Use this for initialization
	void Start () {
		StartTime ();
	}

	// Update is called once per frame
	void Update () {
		UpdateTime ();

		#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER || UNITY_WEBGL

		// we only need to process if we have some interesting input this frame
		if( Input.GetMouseButtonUp( 0 ) || Input.GetMouseButton( 0 ) ) {
			ProcessScreenInput (Input.mousePosition);
		}

		#endif

		if( Input.touchCount > 0 ) {
			var maxTouchIndexToExamine = Mathf.Min( Input.touches.Length, kTotalTouchesToProcess );
			for (var i = 0; i < maxTouchIndexToExamine; i++) {
				var touch = Input.touches[i];
				ProcessScreenInput (touch.position);
			}
		}
	}

	void ProcessScreenInput (Vector3 position) {

		var ray = Camera.main.ScreenPointToRay(position);
		bool hasHit = false;
		RaycastHit2D hit2d = Physics2D.GetRayIntersection(ray,Mathf.Infinity);

		if (hit2d.collider) {
			GameObject go = hit2d.collider.gameObject;
			if (objectSet.Contains(go) && go.activeSelf) {
				hit2d.collider.gameObject.SetActive (false);
				hasHit = true;

				if (OnPop != null && OnPop.GetInvocationList ().Length > 0) {
					OnPop.Invoke ();
				}
			}
		}

		if (hasHit)
			CheckEvents ();
	}

	void CheckEvents () {
		bool hasActive = false;
		foreach (GameObject go in objectSet) {
			if (go.activeSelf)
				hasActive = true;
		}

		if (!hasActive) {
			OnConditionSuccessEvent ();
		}
	}

	public override void OnTimeEndEvent () {
		OnConditionFailEvent ();
	}
}
