﻿using UnityEngine;
using System.Collections;

public class ArmKetongLetter : MonoBehaviour {

	public int GetKetongCount () {
		ArmKetongSection[] ketongs = transform.GetComponentsInChildren<ArmKetongSection>();
		int count = 0;

		foreach (ArmKetongSection section in ketongs) {
			count += section.GetKetongCount ();
			Debug.Log ("ketong count :" + section.GetKetongCount ());
		}

		return count;
	}
}
