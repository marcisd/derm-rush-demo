﻿using UnityEngine;
using System.Collections;
using System;

public class ArmMinigame : AbstractMinigame {

	public GameObject letterContainer;

	public Action OnBrush;

	bool _checkEvents;

	// Use this for initialization
	void Start () {
		StartTime ();

		_checkEvents = false;
	}
	
	// Update is called once per frame
	void Update () {
		UpdateTime ();

		if (_checkEvents) {
			bool hasActive = false;
			foreach (ArmKetongLetter letter in letterContainer.GetComponentsInChildren <ArmKetongLetter> ()) {
				if (letter.GetKetongCount () > 0)
					hasActive = true;
			}

			if (!hasActive) {
				Debug.Log ("ohinde");
				OnConditionSuccessEvent ();
			}
		}
	}

	public override void OnTimeEndEvent () {
		OnConditionFailEvent ();
	}

	public void TryEraseKetong (Vector2 position) {

		if (!acceptsInput)
			return;

		var ray = Camera.main.ScreenPointToRay(position);
		RaycastHit2D hit2d = Physics2D.GetRayIntersection(ray,Mathf.Infinity);

		bool hasHit = false;

		if (hit2d.collider) {
			if (hit2d.collider.gameObject.tag == "Ketong") {
				ArmKetong ak = hit2d.collider.gameObject.GetComponent <ArmKetong> ();
				if (ak.isEnd) {

					if (OnBrush != null && OnBrush.GetInvocationList ().Length > 0) {
						OnBrush.Invoke ();
					}

					ak.DestroyIfEnd ();

					hasHit = true;
				}
			}
		}

		if (hasHit) {
			_checkEvents = true;
		}
	}

	public void TouchBegan (int idx, Vector2 position) {

		TryEraseKetong (position);
	}

	public void TouchMoved (int idx, Vector2 position, Vector2 delta) {

		TryEraseKetong (position);
	}

	public void TouchEnded (int idx, Vector2 position) {

	}
}
