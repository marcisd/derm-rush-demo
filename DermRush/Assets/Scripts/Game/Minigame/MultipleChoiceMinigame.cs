﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class MultipleChoiceMinigame : AbstractMinigame {

	public List<GameObject> buttons;
	public GameObject source;

	public Action OnCorrect;
	public Action OnWrong;

	// Use this for initialization
	void Start () {

		float[] starttimes = {0.1f, 0.2f, 0.3f, 0.4f };
		string[] choices = {"First", "Second", "Third", "Fifth"};

		System.Random rng = new System.Random ();

		//shuffle time
		for (int i = starttimes.Length - 1; i >= 0; i--) {
			int rnd = rng.Next (i);
			if (i == rnd)
				continue;
			else {
				float temp = starttimes [i];
				starttimes [i] = starttimes [rnd];
				starttimes [rnd] = temp;
			}
		}

		//shuffle contents
		for (int i = choices.Length - 1; i >= 0; i--) {
			int rnd = rng.Next (i);
			if (i == rnd)
				continue;
			else {
				string temp = choices [i];
				choices [i] = choices [rnd];
				choices [rnd] = temp;
			}
		}

		int j = 0;
		if (source) {
			foreach (GameObject go in buttons) {

				go.GetComponentInChildren <Text> ().text = choices [j];

				if (String.Equals (choices [j], "Second")) {
					go.GetComponent <Button> ().onClick.AddListener (() => { this.RightAnswerButtonPressed (); });
				} else {
					go.GetComponent <Button> ().onClick.AddListener (() => { this.WrongAnswerButtonPressed (); });
				}

				StartCoroutine (TransformFromTo (go.transform, source.transform, go.transform, 0.2f, starttimes [j++]));
			}
		}

		StartTime ();
	}

	// Update is called once per frame
	void Update () {
		UpdateTime ();
	}

	public override void OnTimeEndEvent () {
		OnConditionFailEvent ();
	}

	public void RightAnswerButtonPressed () {

		DisableButtons ();

		if (OnCorrect != null && OnCorrect.GetInvocationList ().Length > 0) {
			OnCorrect.Invoke ();
		}

		OnConditionSuccessEvent ();
	}

	public void WrongAnswerButtonPressed () {

		DisableButtons ();

		if (OnWrong != null && OnWrong.GetInvocationList ().Length > 0) {
			OnWrong.Invoke ();
		}

		OnConditionTimedFailEvent ();
	}

	void DisableButtons () {
		foreach (GameObject go in buttons) {
			go.GetComponent <Button> ().interactable = false;
		}
	}

	IEnumerator TransformFromTo (Transform objectToMove, Transform source, Transform destination, float duration, float sleep) {

		Vector3 sourceScale = source.localScale;
		Vector3 sourcePosition = source.position;

		Vector3 destinationScale = destination.localScale;
		Vector3 destinationPosition = destination.position;

		objectToMove.localScale = sourceScale;
		objectToMove.position = sourcePosition;

		yield return new WaitForSeconds (sleep);

		float startTime = Time.time;
		while (Time.time < startTime + duration) {
			float t = (Time.time - startTime) / duration;
			objectToMove.position = Vector3.Lerp(sourcePosition, destinationPosition, t); // Move objectToMove closer to b
			objectToMove.localScale = Vector3.Lerp(sourceScale, destinationScale, t);
			yield return new WaitForFixedUpdate();         // Leave the routine and return here in the next frame
		}
		objectToMove.position = destinationPosition;
		objectToMove.localScale = destinationScale;
	}
}
