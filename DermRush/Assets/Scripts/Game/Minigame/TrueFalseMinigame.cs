﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class TrueFalseMinigame : AbstractMinigame {
	 
	public Action OnCorrect;
	public Action OnWrong;

	// Use this for initialization
	void Start () {
		StartTime ();
	}

	// Update is called once per frame
	void Update () {
		UpdateTime ();
	}

	public override void OnTimeEndEvent () {
		OnConditionFailEvent ();
	}

	public void TrueButtonPressed () {

		DisableButtons ();

		if (OnCorrect != null && OnCorrect.GetInvocationList ().Length > 0) {
			OnCorrect.Invoke ();
		}

		OnConditionSuccessEvent ();
	}

	public void FalseButtonPressed () {

		DisableButtons ();

		if (OnWrong != null && OnWrong.GetInvocationList ().Length > 0) {
			OnWrong.Invoke ();
		}

		OnConditionTimedFailEvent ();
	}

	void DisableButtons () {
		GameObject.Find ("Button1").GetComponent <Button> ().interactable = false;
		GameObject.Find ("Button2").GetComponent <Button> ().interactable = false;
	}
}
