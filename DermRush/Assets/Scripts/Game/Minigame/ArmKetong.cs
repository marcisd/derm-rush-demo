﻿using UnityEngine;
using System.Collections;

public class ArmKetong : MonoBehaviour {

	public bool isEnd;

	public ArmKetong linkedKetong;

	public void DestroyIfEnd () {
		if (isEnd) {

			float closest = float.MaxValue;
			ArmKetong nextKetong = null;

			// find nearest ketong in parent
			ArmKetong[] children = transform.parent.GetComponentsInChildren<ArmKetong>();
			foreach (ArmKetong child in children) {
				if (child.gameObject == gameObject)
					continue;

				RectTransform rt = this.transform as RectTransform;
				RectTransform crt = child.transform as RectTransform;

				float dist = Vector2.Distance (rt.localPosition, (Vector2) crt.localPosition);

				if (dist < closest) {
					nextKetong = child;
					closest = dist;
				}
			}

			if (nextKetong) {
				Debug.Log ("next ketong found!" + nextKetong.name);
				nextKetong.isEnd = true;
			}

			// if we have a linked ketong
			if (linkedKetong)
				linkedKetong.isEnd = true;
			Destroy (gameObject);
		}
	}
}
