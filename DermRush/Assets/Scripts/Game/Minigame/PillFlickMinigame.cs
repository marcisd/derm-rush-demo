﻿using UnityEngine;
using System.Collections;
using System;

public class PillFlickMinigame : AbstractMinigame {

	public GameObject target1;
	public GameObject target2;
	public GameObject target3;

	public GameObject pillPrefab;

	bool _isHoldingPill;

	int _hitCounter;

	PillScript _pill;

	private readonly Vector3 ORIG_POS = new Vector3 (0f, -4f, 0f);

	public Action OnFlick;
	public Action OnAllergy;

	// Use this for initialization
	void Start () {
		StartTime ();

		_hitCounter = 0;
		target1.GetComponent <RunningMan> ().OnHitByPillEvent += OnHitByPill;
		target2.GetComponent <RunningMan> ().OnHitByPillEvent += OnHitByPill;
		target3.GetComponent <RunningMan> ().OnHitByPillEvent += OnHitByPill;

		Instantiate (pillPrefab, ORIG_POS, Quaternion.identity);

		_isHoldingPill = false;
	}
	
	// Update is called once per frame
	void Update () {
		UpdateTime ();
	}

	public override void OnTimeEndEvent () {
		OnConditionFailEvent ();
	}

	void OnHitByPill () {
		Debug.Log ("HIT EVENT");
		_hitCounter++;

		if (OnAllergy != null && OnAllergy.GetInvocationList ().Length > 0) {
			OnAllergy.Invoke ();
		}


		if (_hitCounter >= 3) {
			OnConditionSuccessEvent ();
		}
	}

	void ReloadPill () {
		GameObject go = Instantiate (pillPrefab, ORIG_POS, Quaternion.identity) as GameObject;
		StartCoroutine(MoveFromToDurationFactor (go.transform, ORIG_POS + (Vector3.down * 2), ORIG_POS, 0.2f));
	}

	IEnumerator MoveFromToDurationFactor (Transform objectToMove, Vector3 source, Vector3 destination, float duration) {
		float startTime = Time.time;
		while (Time.time < startTime + duration) {
			float t = (Time.time - startTime) / duration;
			objectToMove.position = Vector3.Lerp(source, destination, t); // Move objectToMove closer to b
			yield return new WaitForFixedUpdate();         // Leave the routine and return here in the next frame
		}
		objectToMove.position = destination;
	}

	public void TouchBegan (int idx, Vector2 position) {

		if (!acceptsInput)
			return;

		var ray = Camera.main.ScreenPointToRay(position);
		RaycastHit2D hit2d = Physics2D.GetRayIntersection(ray,Mathf.Infinity);

		if (hit2d.collider) {
			if (hit2d.collider.gameObject.tag == "Pill") {
				_isHoldingPill = true;
				_pill = hit2d.collider.gameObject.GetComponent <PillScript> ();
			}
		}

	}
		
	public void TouchEnded (int idx, Vector2 position) {

		if (!acceptsInput)
			return;

		if (_isHoldingPill && _pill) {

			if (OnFlick != null && OnFlick.GetInvocationList ().Length > 0) {
				OnFlick.Invoke ();
			}

			Vector3 worldPos = Camera.main.ScreenToWorldPoint (new Vector3 (position.x, position.y, 0));
			Vector3 direction = worldPos - ORIG_POS;
			if (direction.sqrMagnitude > 4) {
				StartCoroutine (_pill.MoveTo (direction));
				ReloadPill ();
			}
			_isHoldingPill = false;
			_pill = null;
		}
	}
}
