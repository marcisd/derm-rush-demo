﻿using UnityEngine;
using System.Collections;

public class RunningMan : MonoBehaviour {

	public bool flip = false;
	public bool directionRight = true;
	public float speed = 5;

	Animator anim;
	bool _isHit;

	public System.Action OnHitByPillEvent;

	// Use this for initialization
	void Start () {
		anim = GetComponent <Animator> ();
		anim.SetBool ("IsRunning", true);
		_isHit = false;

		if (!directionRight && !flip) {
			Vector3 charScale = transform.localScale;
			charScale.x *= -1;
			transform.localScale = charScale;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (directionRight)
			transform.Translate (Vector3.right * Time.deltaTime * speed);
		else
			transform.Translate (Vector3.left * Time.deltaTime * speed);
	}

	void OnTriggerEnter2D (Collider2D coll) {
		if (_isHit)
			return;

		if (coll.gameObject.tag == "Wall")
			FlipFacing ();
		else if (coll.gameObject.tag == "Pill") {
			anim.SetBool ("IsRunning", false);
			_isHit = true;
			OnHitByPillEvent.Invoke ();

			// hide pill
			coll.gameObject.SetActive (false);
		}
	}

	void FlipFacing()
	{
		directionRight = !directionRight;
		Vector3 charScale = transform.localScale;
		charScale.x *= -1;
		transform.localScale = charScale;
	}
}
