﻿using UnityEngine;
using System.Collections;

public class AbstractMinigame : MonoBehaviour {

	public float timeLimit;
	ProgressBar progressBar;

	[Header ("Educational")]
	public float educationalDuration;
	public GameObject educationalComponent;

	private float _runningTime;
	protected bool acceptsInput;
	bool dontUpdate;

	public virtual void OnTimeEndEvent () {}

	protected void OnConditionFailEvent () {
		GameManager.Instance.CurrentScore = 0;
		Application.LoadLevel ("scn_Transition");
	}

	protected void OnConditionTimedFailEvent () {
		dontUpdate = true;
		acceptsInput = false;
		StartCoroutine (WaitThenTransitionFail ());
	}

	IEnumerator WaitThenTransitionFail () {
		yield return new WaitForSeconds (1f);

		GameManager.Instance.CurrentScore = 0;
		Application.LoadLevel ("scn_Transition");
	}

	protected void OnConditionSuccessEvent () {
		dontUpdate = true;
		acceptsInput = false;
		StartCoroutine (WaitThenTransitionSuccess ());
	}

	IEnumerator WaitThenTransitionSuccess () {
		yield return new WaitForSeconds (1f);

		GameManager.Instance.CurrentScore = 100;
		Application.LoadLevel ("scn_Transition");
	}
		
	public float GetTimeCompletion () {
		return _runningTime / timeLimit;
	}

	protected void StartTime () {
		progressBar = GameObject.Find ("pfb_ProgressBar").GetComponent <ProgressBar> ();

		_runningTime = timeLimit;
		dontUpdate = false;
	}
	
	protected void UpdateTime () {
		if (educationalComponent != null && educationalComponent.activeSelf) {

			educationalDuration -= Time.deltaTime;
			acceptsInput = false;

			if (educationalDuration < 0) {

				GameObject go = GameObject.Find ("pfb_Doctor_Character_Anim");
				if (go)
					go.SetActive (false);

				educationalComponent.SetActive (false);
				acceptsInput = true;
			}

			return;
		}

		acceptsInput = true;

		if (dontUpdate)
			return;

		if (_runningTime > 0) {
			_runningTime -= Time.deltaTime;
		} else {
			_runningTime = 0;
			OnTimeEndEvent ();
		}
		progressBar.progress = GetTimeCompletion ();
	}
}
