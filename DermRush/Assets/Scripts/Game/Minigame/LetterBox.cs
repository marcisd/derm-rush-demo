﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class LetterBox : MonoBehaviour {

	public char Letter {
		get { 
			Text content = gameObject.GetComponentInChildren <Text> ();
			return Char.ToUpper (content.text.ToCharArray () [0]);
		}
		set {
			Text content = gameObject.GetComponentInChildren <Text> ();
			content.text = Char.ToUpper(value).ToString ();
		}
	}

	public bool LetterMatches (char letter) {
		return Letter == Char.ToUpper (letter);
	}
}
