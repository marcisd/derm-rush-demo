﻿using UnityEditor;
using System.Collections;

[CustomEditor(typeof(ProgressBar))]
public class ProgressBarInspector : Editor {

	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		ProgressBar progressBar = ((ProgressBar)target);

		EditorGUILayout.BeginVertical ();

		progressBar.progress = EditorGUILayout.Slider (progressBar.progress, 0f, 1f);

		EditorGUILayout.EndVertical ();
	}

}
