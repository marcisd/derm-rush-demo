﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour {

	public float progress;
	public Image imageIn;
	public Image imageOut;
	
	// Update is called once per frame
	void Update () {
		imageIn.rectTransform.sizeDelta = new Vector2 (imageOut.rectTransform.rect.width * progress, imageOut.rectTransform.rect.height);
	}
}
